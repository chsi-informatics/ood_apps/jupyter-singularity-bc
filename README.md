# Jupyter Batch Connect

Open OnDemand Batch Connect application that will run Jupyter lab or Jupyter notebook from a Singularity container chosen by the user. Note that a container must have the Jupyter capabilities selected by the user (lab or notebook) or the instance will fail to launch. 
